package com.first.question.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.first.question.model.Usuario;

/**
 * Descrição: Interface que implementa Jpa na camada de persistência
 * 
 * @author "Fábio Campêllo"
 * @since 09/03/2019
 * @version 0.0.1
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	/**
	 * Descrição: Verifica se já existe usuário cadastrado com email passado por parâmetro
	 * 
	 * @param String email
	 * @return int
	 */
	@Query(value = "SELECT COUNT(email) FROM usuario WHERE email = :email", nativeQuery = true)
	public int existsUsuarioByEmail(@Param("email") String email);
	
	/**
	 * Descrição: Loga na aplicação conferindo o email e senha se usuário estiver ativo
	 * 
	 * @param String email
	 * @param String senha
	 * @return Usuario usuario
	 */
	@Query(value = "SELECT * FROM usuario WHERE email = :email AND senha = :senha AND status = true", nativeQuery = true)
	public Usuario autenticaUser(@Param("email") String email, @Param("senha") String senha);
	
	/**	
	 * Descrição: Atualizando status usuário para TRUE
	 * 
	 * @param id
	 */
	@Modifying
	@Transactional
	@Query(value = "UPDATE usuario u SET u.status = true WHERE u.id =:id", nativeQuery = true)
	public void updateAtivarStatus(@Param("id") Long id);
	
	/**	Descrição: Atualizando status usuário para FALSE
	 * 
	 * @param id
	 */
	@Modifying
	@Transactional
	@Query(value = "UPDATE usuario u SET u.status = false WHERE u.id = :id", nativeQuery = true)
	public void updateInativarStatus(@Param("id") Long id);
	
	/**
	 * Descrição: Retornando usuário pelo id
	 * @param id
	 * @return Usuario
	 */
	@Query(value = "SELECT * FROM usuario u WHERE u.id = :id", nativeQuery = true)
	public Usuario findUserById(@Param("id") Long id);
	
}
