package com.first.question.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.first.question.model.Cartela;

/**
 * Descrição: Interface que implementa Jpa na camada de persistência
 * 
 * @author "Fábio Campêllo"
 * @since 10/04/2019
 * @version 0.0.1
 */
public interface CartelaRepository extends JpaRepository<Cartela, Long> {
	
}
