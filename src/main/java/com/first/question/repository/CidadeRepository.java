package com.first.question.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.first.question.model.Cidade;

/**
 * Descrição: Interface que implementa Jpa na camada de persistência
 * 
 * @author "Fábio Campêllo"
 * @since 09/04/2019
 * @version 0.0.1
 */
public interface CidadeRepository extends JpaRepository<Cidade, Long> {
	
	/**
	 * Descrição: Retornando cidade pelo id
	 * @param id
	 * @return Cidade
	 */
	@Query(value = "SELECT * FROM cidade u WHERE u.id = :id", nativeQuery = true)
	public Cidade findCidadeById(@Param("id") Long id);

}
