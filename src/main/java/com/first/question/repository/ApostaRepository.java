package com.first.question.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.first.question.model.Aposta;

/**
 * Descrição: Interface que implementa Jpa na camada de persistência
 * 
 * @author "Fábio Campêllo"
 * @since 10/04/2019
 * @version 0.0.1
 */
public interface ApostaRepository extends JpaRepository<Aposta, Long> {
	
	@Query(value = "SELECT * FROM aposta u WHERE u.id_usuario = :id", nativeQuery = true)
	public List<Aposta> getAllApostasByUserId(@Param("id") Long id);
	
}
