package com.first.question.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.first.question.model.Aposta;
import com.first.question.repository.ApostaRepository;

/**
 * Descrição: Classe de serviço de apostas, implementa regra de negócio conforme
 * a demanda e dependendo da necessidade implementa na camada de persistência o
 * JPA ou JDBC.
 * 
 * @author "Fábio Campêllo"
 * @since 12/04/2019
 * @version 0.0.1
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ApostaServiceBean implements ApostaService {
	
	@Autowired
	private ApostaRepository apostaRepository;
	
	/**
	 * Descrição: Busca todos as apostas do usuário logado
	 * 
	 * @return List<Aposta> apostas
	 */
	@Override
	public List<Aposta> getAllApostasByUser(Long id) {
		List<Aposta> list = null;
		try {
			list = apostaRepository.getAllApostasByUserId(id);
		} catch(Exception error) {
			error.printStackTrace();
		}
		return list;
	}
	
}
