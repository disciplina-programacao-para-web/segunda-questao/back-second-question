package com.first.question.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.first.question.POJO.CartelaPOJO;
import com.first.question.business.MegaSenaBusiness;
import com.first.question.model.Aposta;
import com.first.question.model.Cartela;
import com.first.question.repository.ApostaRepository;
import com.first.question.repository.CartelaRepository;

/**
 * Descrição: Classe de serviço de mega-sena, implementa regra de negócio conforme
 * a demanda e dependendo da necessidade implementa na camada de persistência o
 * JPA ou JDBC.
 * 
 * @author "Fábio Campêllo"
 * @since 10/04/2019
 * @version 0.0.1
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class MegaSenaServiceBean implements MegaSenaService {
	
	@Autowired
	private ApostaRepository apostaRepository;
	
	@Autowired
	private CartelaRepository cartelaRepository;
	
	@Override
	public CartelaPOJO updateAposta(Aposta aposta) {
		CartelaPOJO cartelas = null;
		try {
			aposta = apostaRepository.save(aposta);
			cartelas = MegaSenaBusiness.generatedAposta(aposta);
			for(int count = 0; count < cartelas.getCartelas().size(); count++) {
				Cartela cartela = new Cartela();
				cartela = cartelas.getCartelas().get(count);
				cartela.setAposta(aposta);
				cartelaRepository.save(cartela);
			}
		} catch (Exception error) {
			error.printStackTrace();
		}
		return cartelas;
	}

}
