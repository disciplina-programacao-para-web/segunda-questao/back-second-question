package com.first.question.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.first.question.model.Usuario;
import com.first.question.repository.UsuarioRepository;

/**
 * Descrição: Classe de serviço do usuário, implementa regra de negócio conforme
 * a demanda e dependendo da necessidade implementa na camada de persistência o
 * JPA ou JDBC.
 * 
 * @author "Fábio Campêllo"
 * @since 09/03/2019
 * @version 0.0.1
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UsuarioServiceBean implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	/**
	 * Descrição: Cadastrar/atualizar usuário
	 * 
	 * @param Usuario
	 *            usuario
	 * @return Usuario usuario
	 */
	@Override
	public Usuario updateSaveUsuario(Usuario usuario) {
		Usuario user = null;
		try {
			user = usuarioRepository.save(usuario);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return user;
	}

	/**
	 * Descrição: Verifica se já existe usuário cadastrado com email passado por
	 * parâmetro
	 * 
	 * @param String
	 *            email
	 * @return boolean true/false
	 */
	@Override
	public Boolean existsUsuarioByEmail(String email) {
		int count = 0;
		boolean isFalse = false;
		try {
			count = usuarioRepository.existsUsuarioByEmail(email);
			if (count > 0) {
				isFalse = true;
			} else {
				isFalse = false;
			}
		} catch (Exception error) {
			error.printStackTrace();
		}
		return isFalse;
	}

	/**
	 * Descrição: Loga na aplicação conferindo o email e senha se usuário estiver
	 * ativo
	 * 
	 * @param String
	 *            email
	 * @param String
	 *            senha
	 * @return Usuario usuario
	 */
	@Override
	public Usuario autenticaUser(String email, String senha) {
		Usuario user = null;
		try {
			user = usuarioRepository.autenticaUser(email, senha);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return user;
	}

	/**
	 * Descrição: Busca todos os usuários da base
	 * 
	 * @return List<Usuario> usuario
	 */
	@Override
	public List<Usuario> getAllUsuarios() {
		List<Usuario> list = null;
		try {
			list = usuarioRepository.findAll();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	}

	/**
	 * Descrição: Atualizando status do usuário
	 * 
	 * @param status
	 * @param id
	 */
	@Override
	public void updateStatus(boolean status, Long id) {
		try {
			if (status) {
				usuarioRepository.updateInativarStatus(id);
			} else {
				usuarioRepository.updateAtivarStatus(id);
			}
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	/**
	 * Descrição: Removendo usuário
	 * 
	 * @param id
	 */
	@Override
	public void removeUsuario(Long id) {
		try {
			usuarioRepository.deleteById(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	/**
	 * Descrição: Retornando usuário pelo id
	 * 
	 * @param id
	 * @return Usuario
	 */
	@Override
	public Usuario findUserById(Long id) {
		Usuario user = null;
		try {
			user = usuarioRepository.findUserById(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return user;
	}

	/**
	 * Descrição: Editando o usuário
	 * 
	 * @param usuario
	 * @return usuario
	 */
	@Override
	public Usuario updateUser(Usuario usuario) {
		Usuario user = null;
		try {
			user = usuarioRepository.save(usuario);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return user;
	}

}
