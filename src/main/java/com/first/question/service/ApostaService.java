package com.first.question.service;

import java.util.List;

import com.first.question.model.Aposta;

/**
 * Descrição: Interface de serviço de apostas.
 * 
 * @author "Fábio Campêllo"
 * @since 12/04/2019
 * @version 0.0.1
 */
public interface ApostaService {

	List<Aposta> getAllApostasByUser(Long id);
	
}
