package com.first.question.service;

import java.util.List;

import com.first.question.model.Cartela;

/**
 * Descrição: Interface de serviço de cartelas.
 * 
 * @author "Fábio Campêllo"
 * @since 14/04/2019
 * @version 0.0.1
 */
public interface CartelaService {
	
	List<Cartela> getAllCartelas();

}
