package com.first.question.service;

import java.util.List;

import com.first.question.model.Usuario;

/**
 * Descrição: Interface de serviço do usuário.
 * 
 * @author "Fábio Campêllo"
 * @since 09/03/2019
 * @version 0.0.1
 */
public interface UsuarioService {
	
	Usuario updateSaveUsuario(Usuario usuario);
	
	Boolean existsUsuarioByEmail(String email);
	
	Usuario autenticaUser(String email, String senha); 
	
	List<Usuario> getAllUsuarios();
	
	void updateStatus(boolean status, Long id);
	
	void removeUsuario(Long id);
	
	Usuario findUserById(Long id);
	
	Usuario updateUser(Usuario usuario);

}
