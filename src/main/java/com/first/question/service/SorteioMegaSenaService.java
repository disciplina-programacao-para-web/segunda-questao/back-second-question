package com.first.question.service;

import com.first.question.model.SorteioMegaSena;

/**
 * Descrição: Interface de serviço de sorteio da mega-sena.
 * 
 * @author "Fábio Campêllo"
 * @since 14/04/2019
 * @version 0.0.1
 */
public interface SorteioMegaSenaService {
	
	SorteioMegaSena save(SorteioMegaSena sorteioMegaSena);

}
