package com.first.question.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.first.question.model.Cartela;
import com.first.question.repository.CartelaRepository;

/**
 * Descrição: Classe de serviço das cartelas, implementa regra de negócio conforme
 * a demanda e dependendo da necessidade implementa na camada de persistência o
 * JPA ou JDBC.
 * 
 * @author "Fábio Campêllo"
 * @since 14/04/2019
 * @version 0.0.1
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CartelaServiceBean implements CartelaService {
	
	@Autowired
	private CartelaRepository cartelaRepository;

	/**
	 * Descrição: Busca todas as cartelas
	 * 
	 * @return List<Cartela> cartelas
	 */
	@Override
	public List<Cartela> getAllCartelas() {
		List<Cartela> list = null;
		try {
			list = cartelaRepository.findAll();
		} catch(Exception error) {
			error.printStackTrace();
		}
		return list;
	}
	
}
