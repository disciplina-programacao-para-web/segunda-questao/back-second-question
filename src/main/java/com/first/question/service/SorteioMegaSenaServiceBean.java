package com.first.question.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.first.question.model.SorteioMegaSena;
import com.first.question.repository.SorteioMegaSenaRepository;

/**
 * Descrição: Classe de serviço do sorteio da mega-sena, implementa regra de negócio conforme
 * a demanda e dependendo da necessidade implementa na camada de persistência o
 * JPA ou JDBC.
 * 
 * @author "Fábio Campêllo"
 * @since 14/04/2019
 * @version 0.0.1
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SorteioMegaSenaServiceBean  implements SorteioMegaSenaService {
	
	@Autowired
	private SorteioMegaSenaRepository sorteioMegaSenaRepository;
	
	@Override
	public SorteioMegaSena save(SorteioMegaSena sorteioMegaSena) {
		SorteioMegaSena sms = null;
		try {
			sms = sorteioMegaSenaRepository.save(sorteioMegaSena);
		} catch(Exception error) {
			error.printStackTrace();
		}
		return sms;
	}
	
}
