package com.first.question.service;

import java.util.List;

import com.first.question.model.Cidade;

/**
 * Descrição: Interface de serviço do cidade.
 * 
 * @author "Fábio Campêllo"
 * @since 09/04/2019
 * @version 0.0.1
 */
public interface CidadeService {
	
	Cidade updateSaveCidade(Cidade cid);
	
	List<Cidade> getAllCidades();
	
	void removeCidade(Long id);
	
	Cidade findCidadeById(Long id);

}
