package com.first.question.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.first.question.model.Cidade;
import com.first.question.repository.CidadeRepository;

/**
 * Descrição: Classe de serviço do cidade, implementa regra de negócio conforme
 * a demanda e dependendo da necessidade implementa na camada de persistência o
 * JPA ou JDBC.
 * 
 * @author "Fábio Campêllo"
 * @since 09/04/2019
 * @version 0.0.1
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CidadeServiceBean implements CidadeService {
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	/**
	 * Descrição: Cadastrar/atualizar cidade
	 * 
	 * @param Cidade cidade
	 * @return Cidade cidade
	 */
	@Override
	public Cidade updateSaveCidade(Cidade cid) {
		Cidade cidade = null;
		try {
			cidade = cidadeRepository.save(cid);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return cidade;
	}
	
	/**
	 * Descrição: Busca todos as cidades da base
	 * 
	 * @return List<Cidade> cidades
	 */
	@Override
	public List<Cidade> getAllCidades() {
		List<Cidade> list = null;
		try {
			list = cidadeRepository.findAll();
		} catch(Exception error) {
			error.printStackTrace();
		} 
		return list;
	}
	
	/**
	 * Descrição: Removendo cidade
	 * 
	 * @param id
	 */
	@Override
	public void removeCidade(Long id) {
		try {
			cidadeRepository.deleteById(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	/**
	 * Descrição: Retornando usuário pelo id
	 * 
	 * @param id
	 * @return Cidade
	 */
	@Override
	public Cidade findCidadeById(Long id) {
		Cidade cidade = null;
		try {
			cidade = cidadeRepository.findCidadeById(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return cidade;
	}

}
