package com.first.question.service;

import com.first.question.POJO.CartelaPOJO;
import com.first.question.model.Aposta;

/**
 * Descrição: Interface de serviço de mega-sena.
 * 
 * @author "Fábio Campêllo"
 * @since 10/04/2019
 * @version 0.0.1
 */
public interface MegaSenaService {
	
	CartelaPOJO updateAposta(Aposta aposta);

}
