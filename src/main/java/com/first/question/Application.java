package com.first.question;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableScheduling
@SpringBootApplication
@EnableTransactionManagement
public class Application extends SpringBootServletInitializer {

	public static void main(String[] args) {
		try {
			SpringApplication.run(Application.class, args);
		} catch (ExceptionInInitializerError err) {
			System.out.println(err.getMessage());
		} catch (Exception err) {
			System.err.println(err.getMessage());
		}
	}

}
