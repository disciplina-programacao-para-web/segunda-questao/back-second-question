package com.first.question.POJO;

import java.util.List;

import com.first.question.model.Cartela;

/**
 * Descrição: Classe responsavel por montar o objeto lista de cartelas para ser exibido no front
 * 
 * @author "Fábio Campêllo"
 * @since 10/04/2019
 * @version 0.0.1
 */
public class CartelaPOJO {

	/**
	 * ATRIBUTOS DE CLASSE
	 */
	private List<Cartela> cartelas;

	/**
	 * MÉTODOS ACESSORES
	 */
	public List<Cartela> getCartelas() {
		return cartelas;
	}

	public void setCartelas(List<Cartela> cartelas) {
		this.cartelas = cartelas;
	}

	/**
	 * MÉTODO TOSTRING
	 */
	@Override
	public String toString() {
		return "CartelaDTO [cartelas=" + cartelas + "]";
	}

	/**
	 * HASHCODE
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cartelas == null) ? 0 : cartelas.hashCode());
		return result;
	}

	/**
	 * EQUALS
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartelaPOJO other = (CartelaPOJO) obj;
		if (cartelas == null) {
			if (other.cartelas != null)
				return false;
		} else if (!cartelas.equals(other.cartelas))
			return false;
		return true;
	}

}
