package com.first.question.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.first.question.model.Usuario;
import com.first.question.service.UsuarioService;

/**
 * Descrição: End-point da api responsável por requisicoes do usuário.
 * 
 * @author "Fábio Campêllo"
 * @since 09/03/2019
 * @version 0.0.1
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	/**
	 * Descrição: Cadastrar/atualizar usuário
	 * 
	 * @param Usuario usuario
	 * @return Usuario usuario
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "updateSaveUsuario", method = RequestMethod.POST)
	public ResponseEntity<Usuario> updateSaveUsuario(@RequestBody Usuario usuario) {
		Usuario userRetorno = null;
		boolean existsUser = false;
		try {
			existsUser = usuarioService.existsUsuarioByEmail(usuario.getEmail());
		} catch (Exception error) {
			error.printStackTrace();
		} finally {
			if(!existsUser) {
				userRetorno = usuarioService.updateSaveUsuario(usuario);
			} 
			return new ResponseEntity<Usuario>(userRetorno, HttpStatus.OK);
		}

	}

	/**
	 * Descrição: Verifica se já existe usuário cadastrado com email passado por parâmetro
	 * 
	 * @param String email
	 * @return boolean true/false
	 */
	@RequestMapping(value = "existsUsuarioByEmail/{email}", method = RequestMethod.GET)
	public Boolean existsUsuarioByEmail(@PathVariable("email") String email) {
		Boolean isUser = false;
		try {
			isUser = usuarioService.existsUsuarioByEmail(email);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return isUser;
	}
	
	/**
	 * Descrição: Loga na aplicação conferindo o email e senha se usuário estiver ativo
	 * 
	 * @param String email
	 * @param String senha
	 * @return Usuario usuario
	 */
	@RequestMapping(value = "autenticaUser", method = RequestMethod.POST)
	public ResponseEntity<Usuario> autenticaUser(@RequestBody Usuario usuario) {
		boolean existsUser = usuarioService.existsUsuarioByEmail(usuario.getEmail());
		Usuario user = null;
		if(existsUser) {
			try {
				user = usuarioService.autenticaUser(usuario.getEmail(), usuario.getSenha());
			} catch(Exception error) {
				error.printStackTrace();
			}
		} 
		return new ResponseEntity<Usuario>(user, HttpStatus.OK);
	}
	
	/**
	 * Descrição: Busca todos os usuários da base
	 * 
	 * @return List<Usuario> usuario
	 */
	@RequestMapping(value = "getAllUsuarios", method = RequestMethod.GET)
	public List<Usuario> getAllUsuarios() {
		List<Usuario> list = null;
		try {
			list = usuarioService.getAllUsuarios();
		} catch(Exception error) {
			error.printStackTrace();
		} 
		return list;
	}
	
	/**
	 *  Descrição: Atualizando status do usuário
	 *  
	 * @param status
	 * @param id
	 */
	@RequestMapping(value = "updateStatus/{status}/{id}", method = RequestMethod.GET)
	public void updateStatus(@PathVariable("status") boolean status, @PathVariable("id") Long id) {
		try {
			usuarioService.updateStatus(status, id);
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
	
	/**
	 * Descrição: Removendo usuário
	 * 
	 * @param id
	 */
	@RequestMapping(value = "removeUsuario/{id}", method = RequestMethod.DELETE)
	public void removeUsuario(@PathVariable("id") Long id) {
		try {
			usuarioService.removeUsuario(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
	
	/**
	 * Descrição: Retornando usuário pelo id
	 * @param id
	 * @return Usuario
	 */
	@RequestMapping(value = "findUserById/{id}", method = RequestMethod.GET)
	public ResponseEntity<Usuario> findUserById(@PathVariable("id") Long id) {
		ResponseEntity<Usuario> user = null;
		try {
			user = new ResponseEntity<Usuario>(usuarioService.findUserById(id), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return user;
	}
	
	/**
	 *  Descrição: Editando o usuário
	 *  
	 * @param usuario
	 * @return usuario
	 */
	@RequestMapping(value = "updateUser", method = RequestMethod.POST)
	public ResponseEntity<Usuario> updateUser(@RequestBody Usuario usuario) {
		ResponseEntity<Usuario> user = null;
		try {
//			usuario.setSenha(usuarioService.getSenha(usuario.getId()));		
			user = new ResponseEntity<Usuario>(usuarioService.updateUser(usuario), HttpStatus.OK);
		} catch (Exception error) {
			System.out.println(error.getMessage());
		}
		return user;
	}

}
