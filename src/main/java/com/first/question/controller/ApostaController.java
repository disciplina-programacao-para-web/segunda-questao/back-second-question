package com.first.question.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.first.question.model.Aposta;
import com.first.question.service.ApostaService;

/**
 * Descrição: End-point da api responsável por requisicoes de apostas.
 * 
 * @author "Fábio Campêllo"
 * @since 12/04/2019
 * @version 0.0.1
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/apostas")
public class ApostaController {

	@Autowired
	private ApostaService apostaService;
	
	/**
	 * Descrição: Busca todos as apostas do usuário logado
	 * 
	 * @return List<Aposta> apostas
	 */
	@RequestMapping(value = "getAllApostasByUser/{id}", method = RequestMethod.GET)
	public List<Aposta> getAllApostasByUser(@PathVariable("id") Long id) {
		List<Aposta> list = null;
		try {
			list = apostaService.getAllApostasByUser(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	}
	
}
