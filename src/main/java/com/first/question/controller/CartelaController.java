package com.first.question.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.first.question.model.Cartela;
import com.first.question.service.CartelaService;

/**
 * Descrição: End-point da api responsável por requisicoes das cartelas.
 * 
 * @author "Fábio Campêllo"
 * @since 14/04/2019
 * @version 0.0.1
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/cartelas")
public class CartelaController {
	
	@Autowired
	private CartelaService cartelaService;

	/**
	 * Descrição: Busca todas as cartelas
	 * 
	 * @return List<Cartela> cartelas
	 */
	@RequestMapping(value = "getAllCartelas", method = RequestMethod.GET)
	public List<Cartela> getAllCartelas() {
		List<Cartela> list = cartelaService.getAllCartelas();
		return list;
	}
	
}
