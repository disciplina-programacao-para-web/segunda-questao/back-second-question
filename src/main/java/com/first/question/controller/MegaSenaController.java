package com.first.question.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.first.question.POJO.CartelaPOJO;
import com.first.question.model.Aposta;
import com.first.question.service.MegaSenaService;

/**
 * Descrição: End-point da api responsável por requisições da aposta de mega-sena.
 * 
 * @author "Fábio Campêllo"
 * @since 10/04/2019
 * @version 0.0.1
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/megasena")
public class MegaSenaController {
	
	@Autowired
	private MegaSenaService megaSenaService;

	/**
	 * Descrição: Realizar aposta mega-sena
	 * 
	 * @param Aposta aposta
	 * @return Cartela cartela
	 */
	@RequestMapping(value = "updateAposta", method = RequestMethod.POST)
	public ResponseEntity<CartelaPOJO> updateAposta(@RequestBody Aposta aposta) {
		CartelaPOJO cartelas = megaSenaService.updateAposta(aposta);
		return new ResponseEntity<CartelaPOJO>(cartelas, HttpStatus.OK);
	}
	
}
