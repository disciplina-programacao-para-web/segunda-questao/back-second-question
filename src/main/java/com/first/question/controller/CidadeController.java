package com.first.question.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.first.question.model.Cidade;
import com.first.question.service.CidadeService;

/**
 * Descrição: End-point da api responsável por requisicoes de cidade.
 * 
 * @author "Fábio Campêllo"
 * @since 09/04/2019
 * @version 0.0.1
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/cidades")
public class CidadeController {

	@Autowired
	private CidadeService cidadeService;

	/**
	 * Descrição: Cadastrar/atualizar cidade
	 * 
	 * @param Cidade cidade
	 * @return Cidade cidade
	 */
	@RequestMapping(value = "updateSaveCidade", method = RequestMethod.POST)
	public ResponseEntity<Cidade> updateSaveCidade(@RequestBody Cidade cid) {
		Cidade cidade = cidadeService.updateSaveCidade(cid);
		return new ResponseEntity<Cidade>(cidade, HttpStatus.OK);
	}

	/**
	 * Descrição: Busca todos as cidades da base
	 * 
	 * @return List<Cidade> cidades
	 */
	@RequestMapping(value = "getAllCidades", method = RequestMethod.GET)
	public List<Cidade> getAllCidades() {
		List<Cidade> list = null;
		try {
			list = cidadeService.getAllCidades();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	}
	
	/**
	 * Descrição: Removendo cidade
	 * 
	 * @param id
	 */
	@RequestMapping(value = "removeCidade/{id}", method = RequestMethod.DELETE)
	public void removeUsuario(@PathVariable("id") Long id) {
		try {
			cidadeService.removeCidade(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
	
	/**
	 * Descrição: Retornando cidade pelo id
	 * @param id
	 * @return cidade
	 */
	@RequestMapping(value = "findCidadeById/{id}", method = RequestMethod.GET)
	public ResponseEntity<Cidade> findCidadeById(@PathVariable("id") Long id) {
		ResponseEntity<Cidade> cidade = null;
		try {
			cidade = new ResponseEntity<Cidade>(cidadeService.findCidadeById(id), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return cidade;
	}

}
