package com.first.question.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cartela")
public class Cartela implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ATRIBUTOS DE CLASSE
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "dezenas")
	private Integer dezenas;

	@Column
	@ElementCollection(targetClass = Integer.class)
	private List<Integer> numCartela;

	@ManyToOne
	@JoinColumn(name = "id_aposta")
	private Aposta aposta;

	/**
	 * MÉTODOS ACESSORES
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDezenas() {
		return dezenas;
	}

	public void setDezenas(Integer dezenas) {
		this.dezenas = dezenas;
	}

	public List<Integer> getNumCartela() {
		return numCartela;
	}

	public void setNumCartela(List<Integer> numCartela) {
		this.numCartela = numCartela;
	}

	public Aposta getAposta() {
		return aposta;
	}

	public void setAposta(Aposta aposta) {
		this.aposta = aposta;
	}

	/**
	 * CONSTRUTOR PADRÃO
	 */
	public Cartela() {
		super();
	}

	/**
	 * MÉTODO TOSTRING
	 */
	@Override
	public String toString() {
		return "Cartela [id=" + id + ", dezenas=" + dezenas + ", numCartela=" + numCartela + ", aposta=" + aposta + "]";
	}

	/**
	 * HASHCODE
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aposta == null) ? 0 : aposta.hashCode());
		result = prime * result + ((dezenas == null) ? 0 : dezenas.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((numCartela == null) ? 0 : numCartela.hashCode());
		return result;
	}

	/**
	 * EQUALS
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cartela other = (Cartela) obj;
		if (aposta == null) {
			if (other.aposta != null)
				return false;
		} else if (!aposta.equals(other.aposta))
			return false;
		if (dezenas == null) {
			if (other.dezenas != null)
				return false;
		} else if (!dezenas.equals(other.dezenas))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (numCartela == null) {
			if (other.numCartela != null)
				return false;
		} else if (!numCartela.equals(other.numCartela))
			return false;
		return true;
	}

}
