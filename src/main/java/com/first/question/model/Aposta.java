package com.first.question.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "aposta")
public class Aposta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ATRIBUTOS DE CLASSE
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "quant_jogos")
	private Integer quantJogos;

	@NotNull
	@Column(name = "quant_dezenas")
	private Integer quantDezenas;

	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	/**
	 * MÉTODOS ACESSORES
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantJogos() {
		return quantJogos;
	}

	public void setQuantJogos(Integer quantJogos) {
		this.quantJogos = quantJogos;
	}

	public Integer getQuantDezenas() {
		return quantDezenas;
	}

	public void setQuantDezenas(Integer quantDezenas) {
		this.quantDezenas = quantDezenas;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * CONSTRUTOR PADRÃO
	 */
	public Aposta() {
	}
	
	/**
	 * CONSTRUTOR PADRÃO RECEBENDO (QUANTIDADE DE JOGOS, QUANTIDADE DE NÚMEROS)
	 */
	public Aposta(Integer quantJogos, Integer quantDezenas) {
		this.quantJogos = quantJogos;
		this.quantDezenas = quantDezenas;
	}

	/**
	 * MÉTODO TOSTRING
	 */
	@Override
	public String toString() {
		return "Aposta [id=" + id + ", quantJogos=" + quantJogos + ", quantDezenas=" + quantDezenas + ", usuario="
				+ usuario + "]";
	}

	/**
	 * HASHCODE
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((quantDezenas == null) ? 0 : quantDezenas.hashCode());
		result = prime * result + ((quantJogos == null) ? 0 : quantJogos.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	/**
	 * EQUALS
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aposta other = (Aposta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (quantDezenas == null) {
			if (other.quantDezenas != null)
				return false;
		} else if (!quantDezenas.equals(other.quantDezenas))
			return false;
		if (quantJogos == null) {
			if (other.quantJogos != null)
				return false;
		} else if (!quantJogos.equals(other.quantJogos))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

}
