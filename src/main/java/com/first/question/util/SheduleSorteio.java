package com.first.question.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.first.question.business.MegaSenaBusiness;
import com.first.question.model.SorteioMegaSena;
import com.first.question.service.CartelaService;
import com.first.question.service.SorteioMegaSenaService;

/**
 * Descrição: Classe de agendamento de sorteio dos números da megasena.
 * 
 * @author "Fábio Campêllo"
 * @since 14/04/2019
 * @version 0.0.1
 */
@Component
public class SheduleSorteio {

	@Autowired
	private MegaSenaBusiness megaSenaBusiness;
	
	@Autowired
	private CartelaService cartelaService;
	
	@Autowired
	private SorteioMegaSenaService sorteioMegaSenaService;

	/**
	 * ANOTATION QUE DEFINE A CHAMADA DO MÉTODO QUE DEFINE O SORTEIO DA MEGASENA
	 */
	@Scheduled(cron = "0 0/5 * * * *") 
	public void executar() {

		SorteioMegaSena sms = null;
		Integer quantJogos = 1; // VARIÁVEL QUE DEFINE A QUANTIDADE DE JOGOS
		Integer quantDezenas = 6; // VARIÁVEL QUE DEFINE A QUANTIDADE DE NÚMEROS SORTEADOS

		/**
		 * Descrição: Método que sorteia os números da megasena
		 * 
		 * @param Integer quantJogos
		 * @param Integer quantDezenas
		 * 
		 */
		sms = megaSenaBusiness.sorteioMegaSena(quantJogos, quantDezenas);
		System.out.println("Atenção! Sorteio da Mega-Sena realizado!");
		sorteioMegaSenaService.save(sms);
	}

}
