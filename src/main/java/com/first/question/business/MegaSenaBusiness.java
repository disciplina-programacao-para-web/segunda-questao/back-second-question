package com.first.question.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Component;

import com.first.question.POJO.CartelaPOJO;
import com.first.question.model.Aposta;
import com.first.question.model.Cartela;
import com.first.question.model.SorteioMegaSena;

/**
 * Descrição: Classe responsavel pela regra de geração de cartelas e dezenas
 * 
 * @author "Fábio Campêllo"
 * @since 10/04/2019
 * @version 0.0.1
 */
@Component
public class MegaSenaBusiness {

	/**
	 * VARIÁVEL RESPONSÁVEL POR GERAR NÚMEROS RANDOMICOS
	 */
	static Random rand = new Random();

	public static CartelaPOJO generatedAposta(Aposta aposta) {
		CartelaPOJO cartelas = new CartelaPOJO();
		List<Cartela> list = new ArrayList<Cartela>();
		
		List<Integer> listNum = null; // LISTA DE NÚMEROS
		Cartela cartela = null; // CARTELA DE JOGO MEGA-SENA
		
		int count = 0; // CONTADOR DE JOGOS
		do {
			count += 1;
			cartela = new Cartela();
			listNum = new ArrayList<Integer>();
			
			// LAÇO QUE PREENCHE A LISTA DE NÚMEROS COM VALORES ALEATÓRIOS COM INTERVÁLO [1 .. 60]
			for(int dezena = 1; dezena <= aposta.getQuantDezenas(); dezena++) {
				int num = preenchendoCartela();
		
				// VERIFICA SE O VALOR JÁ ESTÁ INSERIDO NA LISTA
				if(!listNum.contains(num)) {
					listNum.add(num);
				}
			}
			Collections.sort(listNum); // ORDENANDO LISTA DE NÚMEROS
			cartela.setDezenas(aposta.getQuantDezenas()); // GUARDANDO QUANTIDADE DE DEZENAS NA CARTELA
			cartela.setNumCartela(listNum); // GUARDANDO LISTA DE NÚMEROS NA CARTELA
			list.add(cartela); // ADICIONANDO CARTELA À LISTA DE CARTELAS
		} while (count < aposta.getQuantJogos());
		
		cartelas.setCartelas(list); // ADICIONANDO À LISTA DE CARTELAS A CARTELAPOJO PARA RETORNAR AO FRONT
		return cartelas;
	}

	/**
	 * Descrição: Método que preenche a cartela com interválos de 1 à 60
	 *  
	 * @return int number
	 */
	private static int preenchendoCartela() {
		int number = 0;
		if(rand.nextInt(60 + 1) != 0) {
			number = rand.nextInt(60 + 1);
		}
		return number;
	}
	
	/**
	 * Descrição: Método que sorteia os números da mega-sena
	 * 
	 * @param quantJogos
	 * @param quantDezenas
	 * @return
	 */
	public static SorteioMegaSena sorteioMegaSena(Integer quantJogos, Integer quantDezenas) {
		Aposta paramAposta = new Aposta(quantJogos, quantDezenas);
		CartelaPOJO numSorteados = generatedAposta(paramAposta);
		System.out.println("--> SORTEIO: " + numSorteados);
		SorteioMegaSena sms = new SorteioMegaSena();
		sms.setDezenas(quantDezenas);
		sms.setNumCartela(numSorteados.getCartelas().get(0).getNumCartela());
		return sms;
	}

}
